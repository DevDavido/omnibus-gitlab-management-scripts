#!/bin/bash
# Example usage:
# ./docker-image-update-check.sh gitlab/gitlab-ce update-gitlab.sh

IMAGE="$1"
COMMAND="$2"

apidomain="registry-1.docker.io"

authdomsvc=`curl --head "https://${apidomain}/v2/" 2>&1 | grep realm | cut -f2- -d "=" | tr "," "?" | tr -d '"' | tr -d "\r"`

# find the number of slashes since registry name and org are not required for images in docker.io
slashes=$(echo $IMAGE | awk -F"/" '{print NF-1}')

if [ "$slashes" == "2" ]; then
reg=$(echo $IMAGE | cut -d / -f 1)
org=$(echo $IMAGE | cut -d / -f 2)
img=$(echo $IMAGE | cut -d / -f 3- | cut -d ":" -f 1)
tag=$(echo $IMAGE | cut -d / -f 3- | cut -d ":" -f 2)
elif [ "$slashes" == "1" ]; then
reg="docker.io"
org=$(echo $IMAGE | cut -d / -f 1)
img=$(echo $IMAGE | cut -d / -f 2- | cut -d ":" -f 1)
tag=$(echo $IMAGE | cut -d / -f 2- | cut -d ":" -f 2)
else
reg="docker.io"
org="library"
img=$(echo $IMAGE | cut -d ":" -f 1)
tag=$(echo $IMAGE | cut -d ":" -f 2)
fi

echo reg: $reg   org: $org   img: $img   tag: $tag

# Set the scope to look for our image
authscope="repository:${org}/${img}:pull"

echo "Fetching Docker Hub token..."
token=$(curl --silent "${authdomsvc}&scope=${authscope}&offline_token=1&client_id=shell" | jq -r '.token')
#echo $token

# Grab the docker image digest from the Docker-Content-Digest: that is in the http header
# Use “Accept: application/vnd.docker.distribution.manifest.v2+json” in order to get the version 2 schema
echo -n "Fetching remote digest... "
digest=$(curl --silent --head -H "Accept: application/vnd.docker.distribution.manifest.v2+json" \
	-H "Authorization: Bearer $token" \
	"https://${apidomain}/v2/${org}/${img}/manifests/latest" | grep -i "Docker-Content-Digest:" | cut -f 2 -d " " | tr -d '"' | tr -d "\r" )
echo "$digest"

echo -n "Fetching local digest...  "
local_digest=$(docker images -q --no-trunc $IMAGE:latest)
echo "$local_digest"

if [ "$digest" != "$local_digest" ] ; then
	echo "Update available. Executing update command..."
	($COMMAND)
else
	echo "Already up to date. Nothing to do."
fi
